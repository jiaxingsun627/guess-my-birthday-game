from random import randint

name = input("Hi! what is your name?")

for guess_number in range(1,5):
    month_number = randint(1,12)
    year_number = randint(1924,2004)

    print ("Guess", guess_number, ":", name ," , were you born in" , 
    month_number, "/", year_number,"?")

    response = input( "yes or no ? ").lower()

    if response == "yes" :
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print (" I have other things to do. Good bye.")

    else :
        print (" Drat! lemme try again!")